# DuoMAG.jl

DuoMAG.jl is a Julia script that reads and analyzes MEP .ascii files produced by DuoMAG TMS stimulator.

The original (no longer maintained) original version was written for Python 3: [DuoMAG.py](https://notabug.org/AdamWysokinski/DuoMAG).

## Installation

Download and unzip [the repository](https://codeberg.org/AdamWysokinski/DuoMAG.jl/archive/master.zip) or clone it using git:

    $ git clone https://notabug.org/AdamWysokinski/DuoMAG.jl

Julia 1.0+ and the following packages are required:

- CSV
- DataFrames
- Plots

## Usage

Example input file is included in the repository: [test.ascii](https://codeberg.org/AdamWysokinski/DuoMAG.jl/src/branch/master/test.ascii).

To analyze the file, run the program as:

    $ ./duomag.jl file_name.ascii > test.log

Below is a typical session:

    DuoMAG MEP analyzer v1.0
    ========================
    Imported packages:
           CSV 0.9.11
    DataFrames 1.3.1
         Plots 1.25.4

    Reading file: test.ascii
    Processing..
    Saving TXT: test-report.txt
    Generating plots..
    Saving CSV: test-signals.csv
    Saving CSV: test-peaks.csv
    Analysis completed.

Example test report (cut for the first signal only):

    Stimulator: Deymed DuoMAG rTMS
    Version: 1.3.6802.30842
    Subject: TEST
    Subject ID: TEST
    Record ID: TEST
    Date: 22.11.2019 10:17:26
    Method: MEP:MT
    Coil type: ----
    Markers latency unit: ms
    Sampling interval: 80
    Sampling interval unit: µs
    Signal resolution: 0.25
    Signal resolution unit: µV
    Number of signals: 20
    Number of signals with undetected markers: 1
    Number of samples per signal: 8129

    Signal: 1
        Stimulation intensity: 60%
        A- latency: 36.08 ms
        A- amplitude: -0.0195 V
        A+ latency: 30.8 ms
        A+ amplitude: 0.0302 V
        Interpeak latency: 5.28 ms
        Peak-to-peak amplitude: 0.0498 V

## TODO

* calculate AUC for A+ and A- peaks

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## History

v1.0 (25/10/2021) First official release.

## Credits

Author: Prof. Adam Wysokiński (adam.wysokinski AT umed.lodz.pl)

Department of Old Age Psychiatry and Psychotic Disorders

Medical University of Lodz, Poland

## License

This software is licensed under [The 2-Clause BSD License](LICENSE).