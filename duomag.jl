#!/usr/bin/env julia

using CSV
using DataFrames
using Pkg
using Plots
using Plots.PlotMeasures
using Statistics

program_version = "1.0"
println("DuoMAG MEP analyzer v$program_version")
println("========================")

m = Pkg.Operations.Context().env.manifest
println("Imported packages:")
println("       CSV $(m[findfirst(v->v.name=="CSV", m)].version)")
println("DataFrames $(m[findfirst(v->v.name=="DataFrames", m)].version)")
println("     Plots $(m[findfirst(v->v.name=="Plots", m)].version)")
println()

length(ARGS) == 0 && throw(ArgumentError("No input file provided! Run the script: duomag.jl <input_file.ascii>"))

file_names = ARGS

for file_name in file_names

    ispath(file_name) || throw(ArgumentError("File $file_name does not exist!"))

    println("Reading file: $file_name")

    try
        f = open(file_name, "r")
    catch
        error("File $file_name cannot be opened!")
    end
    
    _ = readline(f)
    sw_name = split(strip(readline(f)), '=')[2]
    sw_version = split(strip(readline(f)), '=')[2]
    subject = split(strip(readline(f)), '=')[2]
    subject_id = split(strip(readline(f)), '=')[2]
    record_id = split(strip(readline(f)), '=')[2]
    record_created = split(strip(readline(f)), '=')[2]
    method = split(strip(readline(f)), '=')[2]
    _ = readline(f)
    marker_latency_unit = split(strip(readline(f)), '=')[2]
    _ = readline(f)
    sampling_interval = parse(Int, (split(strip(readline(f)), '=')[2]))
    sampling_interval_unit = split(strip(readline(f)), '=')[2]
    resolution = parse(Float64, replace(split(strip(readline(f)), '=')[2], ',' => '.'))
    resolution_unit = split(strip(readline(f)), '=')[2]
    signal_count = parse(Int, split(strip(readline(f)), '=')[2])
    stimulation = parse.(Int, split(strip(readline(f)), ' ')[2:end])
    channel = parse.(Int, split(strip(readline(f)), ' ')[2:end])
    samples_count = parse.(Int, split(strip(readline(f)), ' ')[2:end])
    stim_sample = parse.(Int, split(strip(readline(f)), ' ')[2:end])
    stim_intens = parse.(Int, split(strip(readline(f)), ' ')[2:end])
    coil_type = split(strip(readline(f)), ' ')[2:end]
    _ = readline(f)
    markers_neg = replace.(split(strip(readline(f)), ' ')[2:end], ',' => '.')
    markers_pos = replace.(split(strip(readline(f)), ' ')[2:end], ',' => '.')
    _ = readline(f)
    # data matrix: signals × samples
    mep_signal = zeros((samples_count[1], signal_count))
    for idx in 1:size(mep_signal)[1]
        mep_signal[idx, :] = parse.(Float64, replace.(split(strip(readline(f)), ' '), ',' => '.'))
    end
    close(f)

    println("Processing..")
    # samples to mV
    mep_signal *= resolution
    mep_signal *= (resolution / 1000)
    # samples to ms
    mep_time = range(-1754, samples_count[1] - 1754) .* (sampling_interval / 1000) |> collect 
    mep_time = mep_time[1:end - 1]

    # find peaks for signals with N/A markers
    replace!(markers_neg, "N/A" => "0.0")
    replace!(markers_pos, "N/A" => "0.0")
    markers_neg = parse.(Float64, markers_neg)
    markers_pos = parse.(Float64, markers_pos)
    markers_nans = (markers_neg .== 0.0) .|| (markers_pos .== 0.0)

    # averaged signal
    mep_mean = mean(mep_signal, dims=2)
    markers_pos_mean, _ = findmax(mep_mean[2000:end])
    markers_neg_mean, _ = findmin(mep_mean[2000:end])

    interpeak_latency = zeros(size(mep_signal)[2])
    p2p_amplitude = zeros(size(mep_signal)[2])

    for idx in 1:size(mep_signal)[2]
        if markers_nans[idx] == true
            interpeak_latency[idx] = NaN
            p2p_amplitude[idx] = NaN
        else
            if markers_neg[idx] > markers_pos[idx]
                interpeak_latency[idx] = markers_neg[idx] - markers_pos[idx]
            elseif markers_neg[idx] < markers_pos[idx]
                interpeak_latency[idx] = markers_pos[idx] - markers_neg[idx]
            else
                interpeak_latency[idx] = 0
            end
            p2p_amplitude[idx] = mep_signal[findfirst(isequal(markers_pos[idx]), mep_time), idx] + abs(mep_signal[findfirst(isequal(markers_neg[idx]), mep_time), idx])
       end
    end

    # generate report
    txt_filename = replace(file_name, ".ascii" => "-report.txt")
    println("Saving TXT: $txt_filename")
    f = open(txt_filename, "w")
    write(f, "                          Stimulator: $sw_name\n")
    write(f, "                             Version: $sw_version\n")
    write(f, "                             Subject: $subject\n")
    write(f, "                          Subject ID: $subject_id\n")
    write(f, "                           Record ID: $record_id\n")
    write(f, "                                Date: $record_created\n")
    write(f, "                              Method: $method\n")
    write(f, "                           Coil type: $(coil_type[1])\n")
    write(f, "                Markers latency unit: $marker_latency_unit\n")
    write(f, "                   Sampling interval: $sampling_interval\n")
    write(f, "              Sampling interval unit: $sampling_interval_unit\n")
    write(f, "                   Signal resolution: $resolution\n")
    write(f, "              Signal resolution unit: $resolution_unit\n")
    write(f, "                        # of signals: $signal_count\n")
    write(f, "# of signals with undetected markers: $(sum(markers_nans))\n")
    write(f, "             # of samples per signal: $(samples_count[1])\n")
    for idx in 1:size(mep_signal)[2]
        write(f, "Signal: $idx\n")
        write(f, "\tStimulation intensity: $(stim_intens[idx])%\n")
        if markers_nans[idx] == false
            write(f, "\tA- latency: $(markers_neg[idx]) ms\n")
            write(f, "\tA- amplitude: $(round(mep_signal[(findfirst(isequal(markers_neg[idx]), mep_time)), idx], digits=4)) V\n")
            write(f, "\tA+ latency: $(markers_pos[idx]) ms\n")
            write(f, "\tA+ amplitude: $(round(mep_signal[(findfirst(isequal(markers_pos[idx]), mep_time)), idx], digits=4)) V\n")
            write(f, "\tInterpeak latency: $(round(interpeak_latency[idx], digits=4)) ms\n")
            write(f, "\tPeak-to-peak amplitude: $(round(p2p_amplitude[idx], digits=4)) V\n")
        else
            write(f, "\tA-: not detected\n")
            write(f, "\tA+: not detected\n")
        end
    end
    close(f)

    # generate plots
    println("Generating plots..")
    # butterfly plot
    p1 = plot(mep_time, mep_signal, linewidth=0.5, legend=false, size=(800, 400), margins=10px)
    xlims!(-10, 50)
    ylims!(-1, 1)
    xlabel!("Time [ms]")
    ylabel!("MEP signal [V]")
    title!("Butterfly plot of $(size(mep_signal)[2]) signals")
    # averaged plot
    p2 = plot(mep_time, mep_mean, color=:green, linewidth=0.5, label="", legend=false, size=(800, 400), margins=10px)
    p2 = scatter!(p2, (mep_time[findmin(abs.(mep_mean .- markers_pos_mean))[2][1]], mep_mean[findmin(abs.(mep_mean .- markers_pos_mean))[2][1]]), color=:red, markershape=:x, markersize=5, label="A+", legend=true)
    p2 = scatter!(p2, (mep_time[findmin(abs.(mep_mean .- markers_neg_mean))[2][1]], mep_mean[findmin(abs.(mep_mean .- markers_neg_mean))[2][1]]), color=:blue, markershape=:x, markersize=5, label="A−", legend=true)
    xlims!(-10, 50)
    ylims!(-1, 1)
    xlabel!("Time [ms]")
    ylabel!("MEP signal [V]")
    title!("Averaged plot of $(size(mep_signal)[2]) signals")
    p12_filename = replace(file_name, ".ascii" => "-butterfly_averaged.pdf")
    p12 = plot(p1, p2, layout=(2, 1), size=(800, 800), margins=10px)
    savefig(p12, p12_filename)

    # plot individual signals with markers
    for idx in 1:size(mep_signal)[2]
        p_ind = plot(mep_time, mep_signal[1:end, idx], color=:green, linewidth=0.5, label="", legend=false, size=(800, 400), margins=10px)
        if markers_nans[idx] == false
            p_ind = scatter!(p_ind, (mep_time[(findfirst(isequal(markers_pos[idx]), mep_time))], mep_signal[(findfirst(isequal(markers_pos[idx]),mep_time)), idx]), color=:red, markershape=:x, markersize=5, label="A+", legend=true)
            p_ind = scatter!(p_ind, (mep_time[(findfirst(isequal(markers_neg[idx]), mep_time))], mep_signal[(findfirst(isequal(markers_neg[idx]),mep_time)), idx]), color=:blue, markershape=:x, markersize=5, label="A-", legend=true)
        end
        xlims!(-10, 50)
        ylims!(-1, 1)
        xlabel!("Time [ms]")
        ylabel!("MEP signal [V]")
        title!("Signal: $idx")
        p_ind_filename = replace(file_name, ".ascii" => "-" * string(idx) * ".pdf")
        savefig(p_ind, p_ind_filename)
    end

    csv_filename = replace(file_name, ".ascii" => "-signals.csv")
    println("Saving CSV: $csv_filename")
    mep_signal_df = DataFrame(time=mep_time)
    for idx in 1:size(mep_signal)[2]
        signal_name = "signal_" * string(idx)
        mep_signal_df[!, signal_name] = round.(mep_signal[1:end, idx], digits=4)
    end
    CSV.write(csv_filename, mep_signal_df)

    csv_filename = replace(file_name, ".ascii" => "-peaks.csv")
    println("Saving CSV: $csv_filename")
    mep_peaks_df = DataFrame(signal=1:size(mep_signal)[2])
    peaks = zeros(size(mep_signal)[2], 4)
    for idx in 1:size(mep_signal)[2]
        if markers_nans[idx] == false
            peaks[idx, 1] = markers_pos[idx]
            peaks[idx, 2] = round(mep_signal[(findfirst(isequal(markers_pos[idx]), mep_time)), idx], digits=4)
            peaks[idx, 3] = markers_neg[idx]
            peaks[idx, 4] = round(mep_signal[(findfirst(isequal(markers_neg[idx]), mep_time)), idx], digits=4)
        else
            peaks[idx, 1] = NaN
            peaks[idx, 2] = NaN
            peaks[idx, 3] = NaN
            peaks[idx, 4] = NaN
        end
    end
    mep_peaks_df[!, "apos_latency"] = peaks[1:end, 1]
    mep_peaks_df[!, "apos_amplitude"] = peaks[1:end, 2]
    mep_peaks_df[!, "aneg_latency"] = peaks[1:end, 3]
    mep_peaks_df[!, "aneg_amplitude"] = peaks[1:end, 4]
    CSV.write(csv_filename, mep_peaks_df)

end

println("Analysis completed.")